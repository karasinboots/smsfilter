package com.iskar.smsfilter;

import android.content.Context;
import android.preference.PreferenceManager;

public class PreferenceHandler {
    public static void setGetOne(Context context, String text) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("get_one", text).commit();
    }

    public static void setGetTwo(Context context, String text) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("get_two", text).commit();
    }

    public static void setGetThree(Context context, String text) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("get_three", text).commit();
    }

    public static void setGetFour(Context context, String text) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("get_four", text).commit();
    }

    public static void setDeleteOne(Context context, String text) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("delete_one", text).commit();
    }

    public static void setDeleteTwo(Context context, String text) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("delete_two", text).commit();
    }

    public static void setDeleteThree(Context context, String text) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("delete_three", text).commit();
    }

    public static void setDeleteFour(Context context, String text) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("delete_four", text).commit();
    }

    public static String getGetOne(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("get_one", "");
    }

    public static String getGetTwo(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("get_two", "");
    }

    public static String getGetThree(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("get_three", "");
    }

    public static String getGetFour(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("get_four", "");
    }

    public static String getDeleteOne(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("delete_one", "");
    }

    public static String getDeleteTwo(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("delete_two", "");
    }

    public static String getDeleteThree(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("delete_three", "");
    }

    public static String getDeleteFour(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("delete_four", "");
    }
}
