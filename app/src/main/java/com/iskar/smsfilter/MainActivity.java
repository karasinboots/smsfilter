package com.iskar.smsfilter;

import android.Manifest;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.iskar.smsfilter.service.ReceiverService;

import java.util.List;

import pub.devrel.easypermissions.EasyPermissions;

public class MainActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {
    String[] perms = {Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS, "android.permission.WRITE_SMS"};
    TextView hello;
    EditText getOne;
    EditText getTwo;
    EditText getThree;
    EditText getFour;

    EditText deleteOne;
    EditText deleteTwo;
    EditText deleteThree;
    EditText deleteFour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        hello = findViewById(R.id.hello);
        initEditFields();
        if (isMyServiceRunning(ReceiverService.class)) {
            hello.setText("Service started in background, you may close app");
        } else
            hello.setText("Hmm, service is not running. Did you give permission to app? If yes, try to restart app. If this string appears again, contact Igor.");
        EasyPermissions.requestPermissions(this, "cant work without send sms permission",
                1000, perms);
    }

    private void initEditFields() {
        getOne = findViewById(R.id.editText1);
        getOne.setText(PreferenceHandler.getGetOne(getApplicationContext()));
        getTwo = findViewById(R.id.editText2);
        getTwo.setText(PreferenceHandler.getGetTwo(getApplicationContext()));
        getThree = findViewById(R.id.editText3);
        getThree.setText(PreferenceHandler.getGetThree(getApplicationContext()));
        getFour = findViewById(R.id.editText4);
        getFour.setText(PreferenceHandler.getGetFour(getApplicationContext()));

        deleteOne = findViewById(R.id.editText5);
        deleteOne.setText(PreferenceHandler.getDeleteOne(getApplicationContext()));
        deleteTwo = findViewById(R.id.editText6);
        deleteTwo.setText(PreferenceHandler.getDeleteTwo(getApplicationContext()));
        deleteThree = findViewById(R.id.editText7);
        deleteThree.setText(PreferenceHandler.getDeleteThree(getApplicationContext()));
        deleteFour = findViewById(R.id.editText8);
        deleteFour.setText(PreferenceHandler.getDeleteFour(getApplicationContext()));

        Button get = findViewById(R.id.buttonGet);
        Button delete = findViewById(R.id.buttonDelete);
        get.setOnClickListener(v -> {
            PreferenceHandler.setGetOne(getApplicationContext(), getOne.getText().toString());
            PreferenceHandler.setGetTwo(getApplicationContext(), getTwo.getText().toString());
            PreferenceHandler.setGetThree(getApplicationContext(), getThree.getText().toString());
            PreferenceHandler.setGetFour(getApplicationContext(), getFour.getText().toString());
        });
        delete.setOnClickListener(v -> {
            PreferenceHandler.setDeleteOne(getApplicationContext(), deleteOne.getText().toString());
            PreferenceHandler.setDeleteTwo(getApplicationContext(), deleteTwo.getText().toString());
            PreferenceHandler.setDeleteThree(getApplicationContext(), deleteThree.getText().toString());
            PreferenceHandler.setDeleteFour(getApplicationContext(), deleteFour.getText().toString());
        });
       /* getOne.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                PreferenceHandler.setGetOne(getApplicationContext(), charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        getTwo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                PreferenceHandler.setGetTwo(getApplicationContext(), charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        getThree.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                PreferenceHandler.setGetThree(getApplicationContext(), charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        getFour.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                PreferenceHandler.setGetFour(getApplicationContext(), charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        deleteOne.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                PreferenceHandler.setDeleteOne(getApplicationContext(), charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        deleteTwo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                PreferenceHandler.setDeleteTwo(getApplicationContext(), charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        deleteThree.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                PreferenceHandler.setDeleteThree(getApplicationContext(), charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        deleteFour.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                PreferenceHandler.setDeleteFour(getApplicationContext(), charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });*/
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, MainActivity.this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        Intent myIntent = new Intent(this, ReceiverService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            this.startForegroundService(myIntent);
        } else this.startService(myIntent);
        if (isMyServiceRunning(ReceiverService.class)) {
            hello.setText("Service started in background, you may close app");
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            this.startForegroundService(myIntent);
        } else this.startService(myIntent);
        if (isMyServiceRunning(ReceiverService.class)) {
            hello.setText("Service started in background, you may close app");
        } else
            hello.setText("Hmm, service is not running. Did you give permission to app? If yes, try to restart app. If this string appears again, contact Igor.");
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        EasyPermissions.requestPermissions(this, "cant work without send sms permission",
                1000, this.perms);
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
