package com.iskar.smsfilter.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.widget.Toast;

import com.iskar.smsfilter.MainActivity;
import com.iskar.smsfilter.PreferenceHandler;
import com.iskar.smsfilter.R;
import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.content.ContentValues.TAG;

public class ReceiverService extends Service {
    public static final String CHANNEL_ID = "my_channel_01";
    SmsVerifyCatcher smsVerifyCatcher;
    private Context mContext;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //when we return START_NOT_STICKY, so we can stop it
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
        makeNotification("Service is running");
        smsVerifyCatcher = new SmsVerifyCatcher(mContext, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(final String message, final String number) {
                if (message.toLowerCase().contains(PreferenceHandler.getGetOne(mContext))
                        && message.toLowerCase().contains(PreferenceHandler.getGetTwo(mContext))
                        && message.toLowerCase().contains(PreferenceHandler.getGetThree(mContext))
                        && message.toLowerCase().contains(PreferenceHandler.getGetFour(mContext))) {
                    String code = parseCode(message);
                    Toast.makeText(getApplicationContext(), code, Toast.LENGTH_LONG).show();
                }
                if (message.toLowerCase().contains(PreferenceHandler.getDeleteOne(mContext))
                        && message.toLowerCase().contains(PreferenceHandler.getDeleteTwo(mContext))
                        && message.toLowerCase().contains(PreferenceHandler.getDeleteThree(mContext))
                        && message.toLowerCase().contains(PreferenceHandler.getDeleteFour(mContext))) {
                    new Handler().postDelayed(() -> deleteSMS(mContext, message, number), 10000);
                }
            }
        });
        smsVerifyCatcher.onStart();
    }

    public void deleteSMS(Context ctx, String message, String number) {
        try {
            Uri uriSms = Uri.parse("content://sms");
            Cursor c = ctx.getContentResolver().query(uriSms,
                    new String[]{"_id", "thread_id", "address",
                            "person", "date", "body"}, null, null, null);

            Log.i(TAG, "c count......" + c.getCount());
            if (c != null && c.moveToFirst()) {
                do {
                    long id = c.getLong(0);
                    long threadId = c.getLong(1);
                    String address = c.getString(2);
                    String body = c.getString(5);
                    String date = c.getString(3);
                    Log.e("log>>>", "0>" + c.getString(0) + "1>" + c.getString(1) + "2>" + c.getString(2) + "<-1>" + c.getString(3) + "4>" + c.getString(4) + "5>" + c.getString(5));
//                    Log.e("log>>>", "date" + c.getString(0));

//                    if (body.contains(getResources().getText(R.string.invite_text).toString()) && address.equals(number)) {
                    if (message.equals(body) && address.equals(number)) {
                        // mLogger.logInfo("Deleting SMS with id: " + threadId);
                        int rows = ctx.getContentResolver().delete(Uri.parse("content://sms/" + id), "date=?", new String[]{c.getString(4)});
                        Log.e("log>>>", "Delete success......... rows: " + rows);
                        Log.e("log>>>", "Delete success......... body: " + body);
                    }
                } while (c.moveToNext());
            }

        } catch (Exception e) {
            Log.e("log>>>", e.toString());
            Log.e("log>>>", e.getMessage());
        }
    }

    private String parseCode(String message) {
        Pattern p = Pattern.compile("\\b\\d+\\b");
        Matcher m = p.matcher(message);
        String code = "";
        while (m.find()) {
            code = m.group(0);
        }
        return code;
    }

    private void makeNotification(String message) {
        Intent notificationIntent = new Intent(mContext, MainActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            CharSequence name = getString(R.string.app_name);
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            Notification.Builder builder = new Notification.Builder(mContext, CHANNEL_ID)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(message)
                    .setChannelId(CHANNEL_ID)
                    .setOnlyAlertOnce(true)
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(R.drawable.ic_launcher_foreground);
            Notification notification = builder.build();
            NotificationManager notificationManager =
                    (NotificationManager) mContext.getSystemService(NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(mChannel);
            notificationManager.notify(1000, notification);
            startForeground(1000, notification);
        } else {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(message)
                    .setVibrate(new long[]{0L})
                    .setOnlyAlertOnce(true)
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(R.drawable.ic_launcher_foreground)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);
            Notification notification = builder.build();
            NotificationManagerCompat notificationManager =
                    NotificationManagerCompat.from(mContext);
            notificationManager.notify(1000, notification);
            startForeground(1000, notification);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        smsVerifyCatcher.onStop();
    }
}
